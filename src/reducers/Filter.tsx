import { FilterActionTypes } from "../types/actions";
import moment from "moment";

const filtersReducerDefaultState = {
  startDate: moment().format("YYYY-MM-DD")
};

export const FiltersReducer = (
  state = filtersReducerDefaultState,
  action: FilterActionTypes
) => {
  switch (action.type) {
    case "START_DATE":
      return {
        ...state,
        startDate: action.startDate
      };

    default:
      return state;
  }
};
