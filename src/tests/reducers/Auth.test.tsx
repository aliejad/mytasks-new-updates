import { AuthReducer } from "../../reducers/Auth";
import userInfo from "../fixtures/userInfo";
import { AuthActionTypes } from "../../types/actions";

test("should get user basic info", () => {
  const action: AuthActionTypes = {
    type: "GET_USER_INFO"
  };

  const state = AuthReducer(
    { email: "admin@admin.com", name: "Admin" },
    action
  );
  expect(state).toEqual(userInfo[0]);
});

test("should loggedin state be equal true", () => {
  const action: AuthActionTypes = {
    type: "LOGIN"
  };

  const state = AuthReducer(
    { email: "admin@admin.com", name: "Admin" },
    action
  );
  expect(state).toEqual({
    email: "admin@admin.com",
    name: "Admin",
    isLoggedin: true
  });
});

test("should loggedin state be equal false", () => {
  const action: AuthActionTypes = {
    type: "LOGOUT"
  };

  const state = AuthReducer({}, action);
  expect(state).toEqual({ isLoggedin: false });
});
