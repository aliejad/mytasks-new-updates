import * as React from "react";
import ReactDOM from "react-dom";
import { store } from "./store/configureStore";
import { Provider } from "react-redux";
import "./index.css";
import AppRouter from "./routers/AppRouter";

const state = store.getState();
console.log(state);

const MyApp = () => {
  return (
    <Provider store={store}>
      <AppRouter />
    </Provider>
  );
};

ReactDOM.render(<MyApp />, document.getElementById("root"));
