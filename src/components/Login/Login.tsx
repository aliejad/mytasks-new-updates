import React from "react";
import { Field, reduxForm, InjectedFormProps } from "redux-form";
import submit from "./submit";
import "../../styles/login.css";

interface IProps {
  input: any;
  label: any;
  type: any;
  meta: any;
}

const renderField = ({
  input,
  label,
  type,
  meta: { touched, error }
}: IProps) => (
  <div>
    <div className="error">{touched && error && <span>{error}</span>}</div>
    <label>{label}</label>
    <div>
      <input {...input} placeholder={label} type={type} />
    </div>
  </div>
);

const LoginForm = (props: InjectedFormProps) => {
  const { error, handleSubmit, pristine, reset, submitting } = props;
  return (
    <div className="box-layout">
      <h2>MyTasks</h2>
      <p>Login to manage your Tasks</p>
      <form onSubmit={handleSubmit(submit)}>
        <Field
          name="username"
          type="text"
          component={renderField}
          label="Username"
        />
        <Field
          name="password"
          type="password"
          component={renderField}
          label="Password"
        />
        <div className="error">{error && <strong>{error}</strong>}</div>

        <div>
          <button type="submit" disabled={submitting}>
            Log In
          </button>
          <button
            type="button"
            disabled={pristine || submitting}
            onClick={reset}
          >
            Clear Values
          </button>
        </div>
      </form>
    </div>
  );
};

export default reduxForm({
  form: "LoginForm",
  onSubmit: submit
})(LoginForm);
