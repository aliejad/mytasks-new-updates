import { Dispatch } from "redux";
import { setStartDate } from "../../actions/Filter";

const submitFilter = (values: any, dispatch: Dispatch) => {
  const createdAtMoment = values.createdAt;
  dispatch(setStartDate(createdAtMoment));
};

export default submitFilter;
