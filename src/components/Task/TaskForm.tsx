import React from "react";
import { Field, reduxForm, InjectedFormProps } from "redux-form";
import submitTask from "./submitTask";
import DatePicker, { normalizeDates, formatDates } from "../DatePicker";
import "../../styles/taskForm.css";

interface IProps {
  input: any;
  label: any;
  type: any;
  meta: any;
}

const renderField = ({
  input,
  label,
  type,
  meta: { touched, error }
}: IProps) => (
  <div>
    <div className="error">{touched && error && <span>{error}</span>}</div>

    <label>{label}</label>
    <div>
      <input {...input} placeholder={label} type={type} />
    </div>
  </div>
);

const TaskForm = (props: InjectedFormProps) => {
  const { handleSubmit, pristine, reset, submitting, submitSucceeded } = props;
  console.log(submitSucceeded);
  return (
    <div className="task-form">
      <form onSubmit={handleSubmit(submitTask)}>
        <Field
          name="category"
          type="text"
          component={renderField}
          label="Category"
        />
        <Field
          name="description"
          type="text"
          component={renderField}
          label="Description"
        />
        <div>
          <label>Date</label>
          <Field
            name="createdAt"
            component={DatePicker}
            placeholder="Pick date"
            parse={normalizeDates}
            format={formatDates}
          />
        </div>
        <div>
          <button className="task-form-btn" type="submit" disabled={submitting}>
            Add
          </button>
          <button
            className="task-form-btn"
            type="button"
            disabled={pristine || submitting}
            onClick={reset}
          >
            Clear Values
          </button>
        </div>
        <div className="success">
          {submitSucceeded && (
            <strong>Task has been added Successfully, view it in Tasks</strong>
          )}
        </div>
      </form>
    </div>
  );
};

export default reduxForm({
  form: "TaskForm",
  onSubmit: submitTask
})(TaskForm);
