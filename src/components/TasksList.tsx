import React from "react";
import { NavLink } from "react-router-dom";
import { connect } from "react-redux";
import TaskItem from "./Task/TaskItem";
import { RootState } from "../store/configureStore";
import { getTasksOfToday } from "../selectors/tasks";
import TasksListFilter from "./TaskFilter/TasksListFilter";
import moment from "moment";
import "../styles/tasks.css";

export const TasksList = (props: RootState) => {
  return (
    <div className="task-list">
      <TasksListFilter />
      <NavLink className="nav-item" to="/createtask">
        Create New
      </NavLink>
      {props.tasks.length > 0 ? (
        props.tasks.map(task => {
          return <TaskItem key={task.id} {...task} />;
        })
      ) : (
        <p>"There is no tasks to show"</p>
      )}
    </div>
  );
};

const mapStateToProps = (state: RootState) => ({
  tasks: getTasksOfToday(state.tasks, moment(state.filters.startDate))
});

export default connect(mapStateToProps)(TasksList);
